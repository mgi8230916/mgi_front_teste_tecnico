import { Directive, Input } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';

@Directive({
  selector: '[appEqualTo]',
  providers: [{ provide: NG_VALIDATORS, useExisting: EqualValidatorDirective, multi: true }]
})
export class EqualValidatorDirective implements Validator {
  @Input('appEqualTo') controlName: string;

  validate(control: AbstractControl): ValidationErrors | null {
    const controlToCompare = control.root.get(this.controlName);
    if (controlToCompare && controlToCompare.value !== control.value) {
      return { equalTo: true };
    }
    return null;
  }
}

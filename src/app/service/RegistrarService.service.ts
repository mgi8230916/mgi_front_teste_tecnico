import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegistrarService {

  private baseUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  registrarUsuario(dados: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/usuarios`, dados);
  }

  loginUsuario(dados: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/login`, dados);
  }

}

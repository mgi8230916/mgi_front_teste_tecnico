import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormGroup, FormsModule, ReactiveFormsModule, Validators, AbstractControl, FormControl } from '@angular/forms';
import { RegistrarService } from '../service/RegistrarService.service';

@Component({
  standalone: true,
  selector: 'app-registrar',
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent {
  mensagem: string = '';
  registroSucesso = false;
  registerForm: FormGroup;
  submitted = false;

  constructor(

    private formBuilder: FormBuilder,
    private router: Router,
    private registrarservise: RegistrarService
  ) {
    this.registerForm = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      confirmaSenha: ['', Validators.required],
    }, {
      validator: this.passwordMatchValidator
    });
  }

  ngOnInit() {
    this.registerForm.valueChanges.subscribe(() => {
      this.validateForm();
    });
  }

  validateField(fieldName: string) {
    this.mensagem = ''
    const control = this.registerForm.get(fieldName);
    if (control) {
      control.markAsTouched();
    }
    
  }

  validateForm() {
    Object.keys(this.registerForm.controls).forEach(field => {
      const control = this.registerForm.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      }
    });
  }
  passwordMatchValidator(formGroup: FormGroup) {
    const senhaControl = formGroup.get('senha');
    const confirmaSenhaControl = formGroup.get('confirmaSenha');

    if (senhaControl && confirmaSenhaControl && senhaControl.value !== confirmaSenhaControl.value) {
      confirmaSenhaControl.setErrors({ passwordMismatch: true });
    } else {
      confirmaSenhaControl?.setErrors(null);
    }
  }

  registrar() {

    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    const dados = this.registerForm.value;

    this.registrarservise.registrarUsuario(dados).subscribe(
      (response) => {
        this.mensagem = 'Usuário ' + response.nome  + ' registrado com sucesso !';
        setTimeout(() => {
          this.router.navigate(['/sucesso']);
        }, 5000);
      },
      (error) => {
        if (Array.isArray(error.error)) {
          this.mensagem = error.error[0].message;
        } else {
          this.mensagem = error.error.message;
        }
      }
    );

  }

}
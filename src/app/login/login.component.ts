import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegistrarService } from '../service/RegistrarService.service';

@Component({
  standalone: true,
  selector: 'app-login',
  imports: [CommonModule, FormsModule, ReactiveFormsModule],
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loginSuccess: boolean = false;
  isMouseOver: boolean = false;
  isLoginFormNotEmpty: boolean = false;
  emailErrorMessage: string = '';
  senhaErrorMessage: string = '';
  loginForm: FormGroup;
  isRegistering: boolean = false;
  errorMessage: string = '';
  isButtonDisabled: boolean = false;

  setup() {
    this.loginSuccess = false;
    this.emailErrorMessage = '';
    this.senhaErrorMessage = '';
    this.isRegistering = false;
    this.isButtonDisabled = false;
  }

  constructor(
    private router: Router,
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private registrarservise: RegistrarService) {

    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required]
    });
  }

  toggleRegister() {
    this.isRegistering = !this.isRegistering;
  }

  get isLoginFormEmpty(): boolean {
    const email = this.loginForm.get('email')?.value;
    const senha = this.loginForm.get('senha')?.value;
    return email === '' && senha === '';
  }

  checkLoginFormNotEmpty() {
    const { email, senha } = this.loginForm.value;
    this.isLoginFormNotEmpty = email.trim().length > 0 && senha.trim().length > 0;
  }
  handleMouseOver(): void {
    // Se ambos o email e a senha estiverem vazios, ativa o movimento do botão
    if (this.isLoginFormEmpty) {
      this.isMouseOver = true;
    }
  }
  handleMouseOut(): void {
    // Desativa o movimento do botão
    this.isMouseOver = false;
  }

  authenticate() {
    this.checkLoginFormNotEmpty();
    this.setup();
    const { email, senha } = this.loginForm.value;

    this.registrarservise.loginUsuario({ email, senha }).subscribe(
      (response) => {

        this.loginSuccess = true;
      },
      (error) => {
        if (error.error) {
          if (Array.isArray(error.error)) {
            error.error.forEach((err: any) => {
              if (err.localizedItem === 'email') {
                this.emailErrorMessage = err.message;
              } else if (err.localizedItem === 'senha') {
                this.senhaErrorMessage = err.message;
              }
            });
          } else {
            this.senhaErrorMessage = error.error.message;
          }
        }
      }
    );
  }

  registrar() {
    this.router.navigate(['/registrar']);
  }

  ngOnInit() { }
}
